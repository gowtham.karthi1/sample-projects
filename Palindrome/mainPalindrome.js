var revStr='/reverseString.mjs';
var palind='/isPalindrome.mjs';

function reverse(){
    var str=document.getElementById("text").value;
    import (revStr).then(module => document.getElementById("ans").innerHTML=module.reverseString(str));
}

function palindrome(){
    var str=document.getElementById("text").value;
    import (palind).then(module => {
        if(module.isPalindrome(str)){
            document.getElementById("ansPalind").innerHTML="is a Palindrome";
        }else{
            document.getElementById("ansPalind").innerHTML="is not a Palindrome";
        }
    });
}